package com.ecci.tallerbotones

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    fun changeElements(button: Button,textView: TextView,editText: EditText,color:Int){

        textView.setTextColor(color)
        button.isEnabled = false
        textView.setText(editText.getText().toString())
        editText.setText("")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button1: Button = findViewById(R.id.button)
        val button2: Button = findViewById(R.id.button2)
        val button3: Button = findViewById(R.id.button3)
        val button4: Button = findViewById(R.id.button4)
        val button5: Button = findViewById(R.id.button5)
        val button6: Button = findViewById(R.id.button6)
        val textView = findViewById<TextView>(R.id.textView)
        val editText = findViewById<EditText>(R.id.editText)


        button1.setOnClickListener {
            changeElements(button1,textView, editText,resources.getColor(R.color.rojo))
        }
        button2.setOnClickListener {
            changeElements(button2,textView, editText,resources.getColor(R.color.morado))
        }
        button3.setOnClickListener {
            changeElements(button3,textView, editText,resources.getColor(R.color.azul))

        }
        button4.setOnClickListener {
            changeElements(button4,textView, editText,resources.getColor(R.color.verde))
        }
        button5.setOnClickListener {
            changeElements(button5,textView, editText,resources.getColor(R.color.amarillo))
        }

        button6.setOnClickListener {
            button1.isEnabled = true
            button2.isEnabled = true
            button3.isEnabled = true
            button4.isEnabled = true
            button5.isEnabled = true
            textView.setText(resources.getText(R.string.pregunta))
            textView.setTextColor(resources.getColor(R.color.blanco))

        }

    }

}